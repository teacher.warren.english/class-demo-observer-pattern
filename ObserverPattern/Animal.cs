﻿using System;
using System.Collections.Generic;

namespace ObserverPattern
{
    public class Animal : IObservable
    {
        public IObserver Observers { get; set; }

        public Animal()
        {
            Observers = new List<IObserver>();
        }

        public void Subscribe(IObserver observer)
        {
            Observers.Add(observer);
        }

        public void Unsubscribe(IObserver observer)
        {
            Observers.Remove(observer);
        }
        public void Notify()
        {
            foreach (IObserver observer in Observers)
            {
                observer.Notified();
            }
        }

        public void Eat()
        {
            Console.WriteLine("This animal is eating.");
            Notify();
        }

        public void Move()
        {
            Console.WriteLine("This animal is moving.");
        }
    }
}
