﻿using System;

namespace ObserverPattern
{
    public class Zoologist : IObserver
    {
        public void Notified()
        {
            Console.WriteLine("I saw that animal do something!");
        }
    }
}
