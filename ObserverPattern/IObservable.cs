﻿using System.Collections.Generic;

namespace ObserverPattern
{
    public interface IObservable
    {
        public List<IObserver> Observers { get; set; }
        public void Subscribe(IObserver observer);
        public void Unsubscribe(IObserver observer);
        public void Notify();
    }
}
