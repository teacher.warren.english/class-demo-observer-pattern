﻿namespace ObserverPattern
{
    public interface IObserver
    {
        public void Notified();
    }
}
